from fastapi.testclient import TestClient
from src.main import app

client = TestClient(app)


def test_show():
    response = client.get('/pqr')
    assert response.status_code == 200


def test_create():
    response = client.post('/pqr', json={
        "user": {
            "full_name": "Jhon",
            "last_name": "Connor",
            "email": "jhon.connor@t.com",
            "mobile_phone": 123456789
        },
        "type": "petition",
        "claims": [],
        "answers": []
    })
    assert response.status_code == 200


def test_show_one():
    response = client.get('/pqr')
    code = response.json()[0]["code"]
    response = client.get(f'/pqr/{code}')
    assert response.status_code == 200


def test_create_answer():
    response = client.get('/pqr')
    code = response.json()[0]["code"]
    response = client.post(f'/pqr/{code}/answer', json={
        "description": "answer"
    })
    assert response.status_code == 200


def test_create_claim():
    response = client.get('/pqr')
    code = response.json()[0]["code"]
    response = client.post(f'/pqr/{code}/answer', json={
        "description": "1 claim"
    })
    assert response.status_code == 200
