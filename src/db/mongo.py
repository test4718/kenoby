import pymongo
from src.settings import env


class Connection:

    _connection = None
    db=None 

    def __init__(self):
        try:
            # f"mongodb://{}:{}@{}:{}/{}"
            self._connection = pymongo.MongoClient(host=env('DB_HOST'), port=int(env('DB_PORT')), username=env('DB_USER'),password=env('DB_PASS'), authSource="admin")
            self._connection.server_info()
            self.db = self._connection[env('DB_NAME')]

        except pymongo.errors.ServerSelectionTimeoutError as err:
            print('[DATABASE] Database Error: ', err)

    def disconnect(self):
        """Disconnect of database"""
        try:
            self.conn.close()
        except pymongo.errors.ServerSelectionTimeoutError as err:
            print('[DATABASE] Database Error: ', err)
        finally:
            print('DATABASE: Connection closed.')
