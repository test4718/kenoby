from typing import List
from pydantic import BaseModel, validator
from datetime import datetime
import random
import re


class User(BaseModel):
    full_name: str
    last_name: str
    email: str
    mobile_phone: int

    @validator('email')
    def email_validate(cls, value):
        if not re.match(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b', value):
            raise ValueError("Email Invalid")


class Asnwer(BaseModel):
    created_at: datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    description: str


class Claim(BaseModel):
    created_at: datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    description: str 


class PQR(BaseModel):
    user: User
    type: str
    code: str = f"{random.randint(3, 999999)}{datetime.now().strftime('%Y%m%d%H%M%S')}"
    claims: List[Claim] = []
    answers: List[Asnwer] = []
    created_at: datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    @validator('type')
    def type_validate(cls, value):
        if value not in ['petition','complain']:
            raise ValueError("Type Invalid")
