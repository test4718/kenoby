from fastapi import HTTPException

def json_request(request):
    if not 'content-type' in request.headers:
        response.status_code = 401
        raise HTTPException(status_code=400, detail="Should have header Content-Type with value 'application/json'")