from fastapi.exceptions import HTTPException
from src.db.mongo import Connection
from bson.json_util import dumps
from json import loads
from datetime import datetime


class PQR():

    model = None

    def __init__(self) -> None:
        self.model = Connection().db.pqr

    def show(self):
        return loads(dumps(self.model.find()))

    def show_one(self, code):
        return loads(dumps(self.model.find_one({'code': code})))

    def create(self, pqr):
        self.model.insert_one(pqr.dict())
        return pqr.dict()

    def create_claim(self, code, claim):
        pqr = loads(dumps(self.model.find_one({'code': code})))

        if(not pqr["answers"]):
            raise HTTPException(status_code=422, detail="The pqr still has no answer")

        if(( datetime.now()- datetime.strptime(pqr["created_at"], '%Y-%m-%d %H:%M:%S')).days<=5):
            raise HTTPException(status_code=422, detail="The claim is invalid")

        return self.model.update({'code': code}, {
            '$push': {
                "claims": claim.dict()
            }
        })

    def create_answer(self, code, answer):
        return self.model.update({'code': code}, {
            '$push': {
                "answers": answer.dict()
            }
        })
