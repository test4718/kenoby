import os
from dotenv import load_dotenv
from pathlib import Path

env_path = Path('.') / '.env'
load_dotenv()


def env(prop):
    """Return environment variable with name Input"""
    return os.getenv(prop)
