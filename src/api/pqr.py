from typing import List
from fastapi import APIRouter, Request, Response
from src.core.pqr import PQR as PQR_Core
from src.models.pqr import PQR, Asnwer, Claim


pqr_router = APIRouter()


@pqr_router.get('/pqr', status_code=200)
async def show(request: Request, response: Response):
    return PQR_Core().show()


@pqr_router.get('/pqr/{pqr_id}', status_code=200)
async def find(pqr_id: str, response: Response):
    return PQR_Core().show_one(pqr_id)


@pqr_router.post('/pqr', status_code=200)
async def create(pqr: PQR, response: Response):
    return PQR_Core().create(pqr)


@pqr_router.post('/pqr/{pqr_id}/claim', status_code=200)
async def create_claim(pqr_id: str, claim: Claim, response: Response):
    PQR_Core().create_claim(pqr_id, claim)
    return {}


@pqr_router.post('/pqr/{pqr_id}/answer', status_code=200)
async def create_answer(pqr_id: str, answer: Asnwer, response: Response):
    PQR_Core().create_answer(pqr_id, answer)
    return {}
