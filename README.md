# KENOBY Test 

### Installation

1. **Clone this Repo**
  ```sh
  → git clone
  ```
2. **Modify the environment variables following the business needs, this can be seen in the .env defined.**

#### Using Docker (Docker-Compose)
3. **If there were no error in the previus step(2.), inside the project folder  excute:**
  ```sh
  → docker-compose up --build
  ```

#### Using a virtualenv
3. **If there were no error in the previus step(2.), inside the project folder  excute:**
  ```sh
  → viertualenv env
  # windows
  → source env/Scripts/activate
  # Linux
  → source env/bin/activate

  → pip install -r requirements.txt
  → gunicorn --bind 0.0.0.0:4000 wsgi:app
  ```
  

4. **Enter the web page, on the routes:**
```sh
[host:8000](http://127.0.0.1:8000)

[host:8000](http://localhost:8000)
```

#### Testing API
5. **execute unittest**
  ```
  pytest
  ```

#### API Docs
```
[host:4000/docs](http://127.0.0.1:8000/docs)
[host:4000/docs](http://localhost:8000/docs)
```