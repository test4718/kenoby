FROM python:3.8-alpine

ARG path=/app

WORKDIR ${path}


COPY requirements.txt .

RUN python3 -m pip install -r requirements.txt

COPY . .

CMD ["uvicorn","src.main:app","--host","0.0.0.0","--port","8000"]

